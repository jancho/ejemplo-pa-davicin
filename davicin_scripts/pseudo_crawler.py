import random
import pymysql


connection = pymysql.connect(host='localhost',
                             user='root',
                             password='root',
                             db='sample')

with connection:
    cur = connection.cursor()
    cur.execute("SELECT * FROM product")

    for id, *_ in cur.fetchall():
        cur.execute(f'insert into price (product_id, price, date) values ({id}, {random.randrange(10,70)}, CURRENT_TIMESTAMP)')

