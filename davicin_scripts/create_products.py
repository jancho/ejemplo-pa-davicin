import pymysql

PRODUCTS = (
    ('"pepino"', '"Una verdura respingona"'),
    ('"papaya"', '"Una fruta bacana bacana"'),
    ('"amigo"', '"Ahora con un extra de algodón y relleno"')
)

connection = pymysql.connect(host='localhost',
                             user='root',
                             password='root',
                             db='sample')

with connection:
    cur = connection.cursor()

    for name, desc in PRODUCTS:
        cur.execute(f'INSERT INTO product (name, description) VALUES ({name}, {desc})')
