from django.shortcuts import render
from . import models

# Create your views here.
def index(request):
    products = [
            {'name': p.name, 'desc': p.description, 'prices': p.prices.all()}
        for p in models.Product.objects.all()
    ]

    context = {
        'name': 'Ejemplo de Django',
        'products': products,
    }

    return render(request, 'index.html', context);
