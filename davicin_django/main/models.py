from django.db import models

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=350, null=True, blank=True)

    class Meta:
        db_table = "product"
    
class Price(models.Model):
    product = models.ForeignKey(Product, related_name='prices', on_delete=models.CASCADE)
    price = models.FloatField()
    date = models.DateField(auto_now_add=True)

    class Meta:
        db_table = "price"
